from tgvfunctions import tgvfunctions
from sqlalchemy import create_engine
import pandas as pd
from projectsecrets import USERNAME, PASSWORD, SERVER_ADDRESS, DATABASE, TABLE, PORT
import json
import datetime as dt

## Connecting to the database
url='mssql+pymssql://'+USERNAME+':'+PASSWORD+'@'+SERVER_ADDRESS+':'+PORT+'/'+DATABASE
engine = create_engine(url)

## Green Village Data
SourceTag = '25SC01'

## Translation key
rosetta = pd.read_excel('Rosetta.ods')
rosetta = rosetta.to_json(orient='records')
translation = dict()
for record in json.loads(rosetta):
    translation[record['ChannelTag']] = record

## Construct sql query and request the data from the last 15 minutes.
sqlquery = '''SELECT * FROM {} WHERE ((Timestamp BETWEEN '{}' AND '{}') AND (SourceTag = '{}')) '''
endtime = tgvfunctions.roundTime(utc=True, roundTo=900)
begintime = endtime - dt.timedelta(days=3000)
sqlquery2 = sqlquery.format(TABLE, begintime, endtime, SourceTag)
data = pd.read_sql_query(sqlquery2, con=engine, chunksize=100000)
producer = tgvfunctions.makeproducer()
iteration = 0
for chunk in data:
    iteration += 1
    print(iteration)
    chunk = tgvfunctions.isotoepochms(chunk, "Timestamp")
    for idx, row in chunk.iterrows():
        if row["ChannelTag"] in translation.keys():
            datavalue = row["Value"]
            if ',' in datavalue:
                datavalue = float(datavalue.replace(',','.'))
            else:
                datavalue = float(datavalue)
            value = {
                                    "project_id": "energy",
                                    "application_id": "GMC",
                                    "device_id": translation[row["ChannelTag"]]["device_id"],
                                    "timestamp": row["epochms"],
                                    "measurements": [
                                        {
                                            "measurement_id": row["ChannelTag"],
                                            "measurement_description": translation[row["ChannelTag"]]["measurement_description"] + " with id " + str(int(translation[row["ChannelTag"]]["id"])),
                                            "value": datavalue,
                                            "unit": row["Unit"]
                                        }
                                    ]
                                }
            tgvfunctions.produce_fast(producer, value)
        else:
            print(row["ChannelTag"])
    producer.flush()
