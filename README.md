# GMC data
This repo contains two types of scripts. On one hand, the real_time.py and real_time.sh script are ran every 15 minutes to update the changes.
The old_data.py and old_dat.sh script was used to produce all the old data to kafka. In case one would obtain more information about the other SourceTags, this file can be used as an example as to how to push older data as well. The chunksize of 100000 rows was chosen because a larger chunksize is not possible with the 2 GB of RAM on the particular server this script is running on.
